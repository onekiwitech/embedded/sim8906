# sim8906

## 1.Installation Tool
1. Install repo tool：
- `mkdir ~/bin`
- `curl https://storage.googleapis.com/git-repo-downloads/repo-1 > ~/bin/repo`
- `chmod a+x ~/bin/repo`
- `rm -rf ~/.repoconfig` (remove old config)
- add to **.bashrc** `export PATH=$PATH:~/bin`
- `source ~/.bashrc`
2. Install git：
- `sudo apt-get install git`
- `sudo git config --global user.email "your@example.com"`
- `sudo git config --global user.name "Your Name"`
3. Install software package：
- `sudo apt-get update`
- `sudo apt-get install gnupg flex bison gperf build-essential zip curl zlib1g-dev gcc-multilib g++-multilib libc6-dev-i386 lib32ncurses5-dev x11proto-core-dev libx11-dev lib32z-dev ccache libgl1-mesa-dev libxml2-utils xsltproc unzip tofrodos python-markdown lib32readline6-dev`
4. Set bash to default shell mode：
- `sudo dpkg-reconfigure dash`
- `sudo rm /bin/sh`
- `sudo ln -s /bin/bash /bin/sh`
5. Use **apt-get** command to install openssl, libssl-dev：
- `sudo apt-get install openssl`
- `sudo apt-get install libssl-dev`
6. Use **apt-get** command to install JDK：
- `sudo apt-get install openjdk-8-jdk`

## 2.Download Source Code
1. Create folder：
- $ `mkdir sim8906`
- $ `cd sim8906`
2. Download Source Code Android 10：
- **sim8906**_$_ `repo init -u git://codeaurora.org/platform/manifest.git -b release -m LA.UM.8.6.2.r2-03100-89xx.0.xml --repo-url=git://codeaurora.org/tools/repo.git --repo-branch=caf-stable` (Android 10)
- **sim8906**_$_ `repo sync -j20`
3. Download SIM8906 private code proprietary file：
- **sim8906**_$_ `cd cd vendor/qcom/`
- **sim8906/vendor/qcom**_$_ `git clone https://gitlab.com/onekiwitech/embedded/sim8906-proprietary.git proprietary`
- **sim8906/vendor/qcom**_$_ `cd ../../`
4. Download SIM8906 series patches files：
- **sim8906**_$_ `git clone https://gitlab.com/onekiwitech/embedded/simcom-patch-sim8906.git simcom`
5. Merge patch to the source code：
- **sim8906**_$_ `cd simcom`
- **sim8906/simcom**_$_ `./sim8906.sh`
- **sim8906/simcom**_$_ `cd ../`

## 3.Compile android code
1. Compile code：
- `chmod -R 777 sim8906`
- **sim8906**_$_ `source build/envsetup.sh`
- **sim8906**_$_ `lunch msm8937_32go-userdebug`
- **sim8906**_$_ `make update-api`
- **sim8906**_$_ `make -j20`
2. Compile aboot：
- `make aboot -j20`
- Object file:
<top_dir / out / target / product / msm8937_32go / emmc_appsboot.mbn>
3. Compile kernel:
- `make bootimage -j20`
- Object file:
<top_dir / out / target / product / msm8937_32go / boot.img>

4. Compile system:
- `make systemimage -j20`
- Object file:
<top_dir / out / target / product / msm8937_32go / system.img>

5. Compile userdata：
- `make userdataimage -j20`
- Object file:
<top_dir / out / target / product / msm8937_32go / userdata.img>

6. Compile recovery:
- `make recoveryimage -j20`
- Object file:
<top_dir / out / target / product / msm8937_32go / recovery.img>
